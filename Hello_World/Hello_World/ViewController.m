//
//  ViewController.m
//  Hello_World
//
//  Created by Peter Devlin on 1/23/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

BOOL isHello;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    isHello = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//I got the code for point coordinates from Dima at
//http://stackoverflow.com/questions/10556120/how-to-get-a-cgpoint-from-a-tapped-location
//I got the code to create images on the touch location from ravron at
//http://stackoverflow.com/questions/11438400/best-way-to-make-images-with-tap-functionality-in-xcode
//The code for animating images comes from Simon Ng
//http://www.appcoda.com/ios-programming-animation-uiimageview/
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    // Get the specific point that was touched
    CGPoint point = [touch locationInView:self.view];
    NSLog(@"X location: %f", point.x);
    NSLog(@"Y Location: %f",point.y);
    NSArray *earthNames = @[@"e1.gif", @"e2.gif", @"e3.gif", @"e4.gif", @"e5.gif", @"e6.gif",
                            @"e7.gif", @"e8.gif", @"e9.gif", @"e10.gif", @"e11.gif", @"e12.gif",
                            @"e13.gif", @"e14.gif", @"e15.gif", @"e16.gif", @"e17.gif", @"e18.gif",
                            @"e19.gif", @"e20.gif", @"e21.gif", @"e22.gif", @"e23.gif", @"e24.gif",
                            @"e25.gif", @"e26.gif", @"e27.gif", @"e28.gif", @"e29.gif", @"e30.gif",
                            @"e31.gif", @"e32.gif", @"e33.gif", @"e34.gif", @"e35.gif", @"e36.gif",
                            @"e37.gif", @"e38.gif", @"e39.gif", @"e40.gif", @"e41.gif", @"e42.gif",
                            @"e43.gif", @"e44.gif"];
    NSArray *helloNames = @[@"h.gif", @"he.gif", @"hel.gif", @"hell.gif", @"hello.gif"];
    
    if (isHello) {
        
        point.x += 37;
    
        NSMutableArray *images = [[NSMutableArray alloc] init];
        for (int i = 0; i < helloNames.count; i++) {
            [images addObject:[UIImage imageNamed:[helloNames objectAtIndex:i]]];
        }
    
        // Normal Animation
        UIImageView *animationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(60, 95, 96, 75)];
        animationImageView.animationImages = images;
        animationImageView.animationDuration = 0.42;
        animationImageView.animationRepeatCount = 1;
        // ... and center it at the touch location
        [animationImageView setCenter:point];
        [self.view addSubview:animationImageView];
        [animationImageView startAnimating];
        isHello = NO;
    }
    
    else {
        
        NSMutableArray *images = [[NSMutableArray alloc] init];
        for (int i = 0; i < earthNames.count; i++) {
            [images addObject:[UIImage imageNamed:[earthNames objectAtIndex:i]]];
        }
        
        // Normal Animation
        UIImageView *animationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(60, 95, 90, 90)];
        animationImageView.animationImages = images;
        animationImageView.animationDuration = 0.72;
        animationImageView.animationRepeatCount = 1;
        // ... and center it at the touch location
        [animationImageView setCenter:point];
        [self.view addSubview:animationImageView];
        [animationImageView startAnimating];
        
        isHello = YES;
    }
    

}

@end
