//
//  main.m
//  Hello_World
//
//  Created by Peter Devlin on 1/23/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
